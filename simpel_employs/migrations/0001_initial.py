# Generated by Django 3.2.12 on 2022-03-25 01:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import filer.fields.file
import filer.fields.image
import mptt.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('filer', '0015_auto_20220325_0000'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, choices=[('Mr', 'Mr'), ('Miss', 'Miss'), ('Mrs', 'Mrs'), ('Ms', 'Ms'), ('Dr', 'Dr')], help_text='Treatment Pronouns for the human.', max_length=64, verbose_name='title')),
                ('full_name', models.CharField(max_length=255, verbose_name='Full name')),
            ],
            options={
                'verbose_name': 'Profile',
                'verbose_name_plural': 'Profiles',
                'abstract': False,
                'swappable': 'PROFILE_MODEL',
            },
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('code', models.CharField(max_length=255, verbose_name='Code')),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('lft', models.PositiveIntegerField(editable=False)),
                ('rght', models.PositiveIntegerField(editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='department_upline', to='simpel_employs.department', verbose_name='Upline')),
            ],
            options={
                'verbose_name': 'Department',
                'verbose_name_plural': 'Departments',
            },
        ),
        migrations.CreateModel(
            name='Employment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('note', models.TextField(blank=True, max_length=255, null=True, verbose_name='Note')),
            ],
            options={
                'verbose_name': 'Employment',
                'verbose_name_plural': 'Employments',
            },
        ),
        migrations.CreateModel(
            name='Position',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('is_manager', models.BooleanField(default=False, verbose_name='Is Manager')),
                ('is_co_manager', models.BooleanField(default=False, verbose_name='Is Co-Manager')),
                ('is_active', models.BooleanField(default=True, verbose_name='Is Active')),
                ('employee_required', models.IntegerField(default=1, verbose_name='Employee required')),
                ('lft', models.PositiveIntegerField(editable=False)),
                ('rght', models.PositiveIntegerField(editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(editable=False)),
                ('attachment', filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='position_files', to='filer.file')),
                ('department', mptt.fields.TreeForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='staffs', to='simpel_employs.department', verbose_name='Department')),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='chair_upline', to='simpel_employs.position', verbose_name='Upline')),
            ],
            options={
                'verbose_name': 'Position',
                'verbose_name_plural': 'Positions',
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reg_number', models.PositiveIntegerField(blank=True, editable=False, null=True, verbose_name='Reg number')),
                ('inner_id', models.CharField(blank=True, editable=False, max_length=50, null=True, unique=True, verbose_name='Inner ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='created at')),
                ('eid', models.CharField(max_length=255, null=True, verbose_name='Employee ID')),
                ('date_registered', models.DateField(default=django.utils.timezone.now, verbose_name='Date registered')),
                ('is_active', models.BooleanField(default=True, verbose_name='Active')),
                ('attachment', filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='employee_files', to='filer.file')),
                ('employment', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='simpel_employs.employment', verbose_name='Employment')),
                ('picture', filer.fields.image.FilerImageField(help_text='Employee formal picture.', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='employee_photos', to=settings.FILER_IMAGE_MODEL)),
                ('profile', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.PROFILE_MODEL)),
            ],
            options={
                'verbose_name': 'Employee',
                'verbose_name_plural': 'Employees',
            },
        ),
        migrations.CreateModel(
            name='Chair',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('group', models.CharField(choices=[('struct', 'Structural'), ('func', 'Functional')], db_index=True, default='struct', max_length=6)),
                ('from_date', models.DateField(default=django.utils.timezone.now)),
                ('to_date', models.DateField(blank=True, default=django.utils.timezone.now, null=True)),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='positions', to='simpel_employs.employee', verbose_name='Employee')),
                ('position', mptt.fields.TreeForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='chairmans', to='simpel_employs.position', verbose_name='Position')),
            ],
            options={
                'verbose_name': 'Chair',
                'verbose_name_plural': 'Chairs',
            },
        ),
    ]
